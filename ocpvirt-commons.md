### OCP Virt Commands

- port forwarding
  - port forward to virt-launcher to a mariadb instance on a vm
    ```shell
    oc port-forward virt-launcher-databasevm-4qqj6 33066:3306
    ```
  - in another terminal session
    ```shell
    mysql -u devuser -h 127.0.0.1 -P 33066 -p
    ```

- as a cluster administrator, you can manage and inspect a VM by using virsh commands within the VM's virt-launcher pod and its libvirtd container
  ```shell
  oc exec -it virt-launcher-pod-name -n namespace -- bash
  ```