#### - upload images
- find the cdi uploadproxy endpoint
```bash
$ oc get route cdi-uploadproxy -n openshift-cnv -o jsonpath='{.spec.host}'
```

Ex.
```text
 ➡ oc get route cdi-uploadproxy -n openshift-cnv -o jsonpath='{.spec.host}'
cdi-uploadproxy-openshift-cnv.apps.ocpvirt.sandbox2801.opentlc.com
 ➡
```

```bash
$ virtctl image-upload --image-path=./rhel-9.5-x86_64-kvm.qcow2 pvc rhel95-base \
--storage-class=ocs-storagecluster-ceph-rbd \
--access-mode=ReadWriteMany \
--volume-mode=block \
--size=20G \
--uploadproxy-url=https://cdi-uploadproxy-openshift-cnv.apps.ocpvirt.sandbox2801.opentlc.com:443 \
--insecure
```

- check that the image is properly uploaded
```bash
$ 
```



---